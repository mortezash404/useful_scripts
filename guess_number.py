import random

rand = random.randint(1,9)

guess = 0
attemp = 0

while guess != rand:
    guess = int(input("guess the number: "))
    attemp += 1

    if guess < rand:        
        print("too low!")
    elif guess > rand:
        print("too high!")
    else:    
        print(f"you guess after {attemp} attemp")
        break

    