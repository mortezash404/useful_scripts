import sys

"""    
	get args from cmd
	split by = character
"""
def split_string(str):
    data = str.split("=")
    key,value = data[0],data[1]
    return key,value

key,value = split_string(sys.argv[1])

print(f"key is : {key}, value is : {value}")
