import string
import random

def pass_gen(size=8, chars=string.ascii_letters + string.digits + string.punctuation):
	return "".join(random.choice(chars) for _ in range(size))

print(pass_gen())
print(pass_gen(size=5))
