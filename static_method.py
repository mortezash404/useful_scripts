class Sample(object):
    
    # non static method. need to create object

    def test(self):
        print("by creating obj running")
    
    # static method. dont need to create object
    
    @classmethod
    def save(self):
        print("without creating obj running!")

Sample.save()

sample = Sample()
sample.test()