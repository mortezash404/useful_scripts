"""
	Write a program (using functions!) that asks the user for a long string containing multiple words. Print back to the user the same string, except with the words in backwards order. For example, say I type the string:

  	My name is Michele
	Then I would see the string:

 	 Michele is name My	
"""

def reverse_order(s):
	ss = s.split(" ")
	return " ".join(ss[-i] for i in range(1,len(ss)+1))

print(reverse_order("My name is Michele"))
