import random

a = [random.randint(0,100) for i in range(20)]

b = [random.randint(0,100) for i in range(20)]

# a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
# b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]


def common_list(a, b):
    set_a = set(a)
    set_b = set(b)
    return set_a.intersection(set_b)

print(common_list(a,b))